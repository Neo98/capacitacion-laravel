<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    //
    public $table = 'empleado';
    public $fillable = ['nombre','puesto', 'email','edad','antiguedad','sueldo','moneda_sueldo'];


}
